use std::env;
use actix_web::{ web, Scope, Responder };
use lettre::transport::smtp::authentication::Credentials;
use lettre::{
    transport::smtp::client::{
	Tls,
	TlsParameters,
    },
    Message,
    SmtpTransport,
    Transport,
};

pub fn mail_scope() -> Scope {
    web::scope("/mail")
	.route("/send/secure/{email_body}", web::get().to(send_secure))
	.route("/send/localhost/{email_body}", web::get().to(send_localhost))
}

async fn send_secure(email_body: web::Path<String>) -> String {
    let cred_username: String = env::var("CRED_USERNAME").unwrap();
    let cred_password: String = env::var("CRED_PASSWORD").unwrap();

    let email = Message::builder()
	.from("noreply <noreply@sprachschule-mäurer.de>".parse().unwrap())
	.to("Marc Mäurer <marc.maeurer@pm.me>".parse().unwrap())
	.subject("Secure email")
	.body(email_body.to_string())
	.unwrap();

    let creds = Credentials::new(cred_username.to_string(), cred_password.to_string());

    let mailer: SmtpTransport = SmtpTransport::relay("mail.maeurer.dev")
	.unwrap()
	.credentials(creds)
	.build();

    match mailer.send(&email) {
	Ok(_) => "Email sent successfully!".to_string(),
	Err(e) => format!("Could not send emmail: {:#?}", e), 
    }
}

async fn send_localhost(email_body: web::Path<String>) -> String {
    let cred_username: String = env::var("CRED_USERNAME").unwrap();
    let cred_password: String = env::var("CRED_PASSWORD").unwrap();

    let email = Message::builder()
	.from("noreply <noreply@sprachschule-mäurer.de>".parse().unwrap())
	.to("Marc Mäurer <marc.maeurer@pm.me>".parse().unwrap())
	.subject("Unsecure email")
	.body(email_body.to_string())
	.unwrap();

    let creds = Credentials::new(cred_username.to_string(), cred_password.to_string());

    let mailer: SmtpTransport = SmtpTransport::builder_dangerous("localhost")
	.tls(Tls::Required(TlsParameters::builder("localhost".to_string())
			   // any certificate is trusted for use: Self signed certificates,
			   // Certificates from different hostnames, Expired certificates
			   .dangerous_accept_invalid_certs(true) // error: "self signed certificate"
			   .build()
			   .unwrap())
	)
    // .tls(Tls::Opportunistic)
	.credentials(creds)
	.build();

    // Send the email
    match mailer.send(&email) {
	Ok(_) => "Email sent successfully!".to_string(),
	Err(e) => format!("Could not send emmail: {:#?}", e), 
    }
}
