use std::sync::Mutex;
use actix_web::{ web, HttpServer, App, };
use openssl::ssl::{ SslAcceptor, SslMethod, SslFiletype };
use dotenv::dotenv;

mod scopes;

use scopes::{
    mail_scope::mail_scope,
};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    HttpServer::new(move || {
	App::new()
	    .app_data(app_state.clone())
	    .service(users_scope())
	    .service(mail_scope())
    })
	.bind(("localhost", 3000))?
	.run()
	.await
}
