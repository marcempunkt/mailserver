#!/bin/bash

## Import .env
set -o allexport; source ../.env; set +o allexport

if [[ ! $SSL_KEY_PATH == "" || ! $SSL_CERT_PATH == "" ]]; then
    ## Run this command only when the env var for SSL_PATH_CERT && SSL_PATH_KEY are set
    docker container create \
	   -p 25:25 \
	   --name 'marc.mailserver' \
	   --volume $SSL_KEY_PATH:/etc/postfix/certs/privkey.pem \
	   --volume $SSL_CERT_PATH:/etc/postfix/certs/cert.pem \
	   marc/mailserver-image:latest
else
    ## Create container with self-signed certs
    docker container create \
	   -p 25:25 \
	   --name 'marc.mailserver' \
	   marc/mailserver-image:latest
fi

