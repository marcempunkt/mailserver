#!/bin/bash
set -o allexport; source ./dotenv; set +o allexport

useradd -s /sbin/nologin -m $USERNAME
echo "$USERNAME:$PASSWORD" | chpasswd
