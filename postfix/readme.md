### Postfix configuration guide

## Create Docker image with Postfix etc.

Script for creating a `docker` image with `postfix` & `cyrus-sasl` based of `ubuntu`

TODO:
	1. create user with MAILSERVER_(USERNAME & PASSWORD)
		1.1 get the env vars by asking for input when creating the docker container
			or from a dotenv file
	2. disable root login
	3. PROFIT

```bash
#!/bin/bash

# TOD

docker run --name mailserver-dev \
       -e MAILSERVER_USERNAME=test \
       -e MAILSERVER_PASSWORD=test \
       -p 6000:25 \
       -d ubuntu:22.04

sleep 25s
echo "$(tput setaf 1)Waiting because of reasons... Don't know if I have to..."

# copy all the configuration files to the correct path
# docker cp src/db/create_babbelndb.sql babbelndb:/home/create_babbelndb.sql

# start all the daemons
# docker exec -it babbelndb /bin/sh -c 'mysql -u root --password=mypass < /home/create_babbelndb.sql'

echo "$(tput setaf 2)Successfully created a new mailserver-dev container."
```


## Postfix base configuration

The master daemon uses the `master.cf`  file for its configuration information.  
The `main.cf` is the core of your Postfix configuration. Nearly all configuration changes occur in this file  

1. Set hostname to fully qualified hostname

`/etc/postfix/main.cf`

```
myhostname = mail.molotow.com
```

Start postfix automatically after rebooting (EDIT TO DOCKER VERSION)

```
systemctl enable postfix
```

2. Make sure that your system's aliases database is in the correct format

```
newaliases
```

## SASL | Create a user

Install `cyrus-sasl`

```
paru -S cyrus-sasl
```

Edit the /etc/sasl/smtpd.conf:

```
pwcheck_method: saslauthd
# Don't know what these two are doing
mech_list: PLAIN LOGIN
log_level: 7
```

Start `saslauthd` service with `PAM` as Authentication (EDIT TO DOCKER VERSION)

```
saslauthd -a pam
systemctl enable saslauthd
```

Edit `main.cf` to accept SASL password authentication:

```
smtpd_sasl_auth_enable = yes
smtpd_recipient_restrictions = permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination
# Support for old email clients (may not be needed)
broken_sasl_auth_clients = yes
```



















login into postfix server using telnet

```
echo -en "userxy" | base64
...
echo -en "password" | base64
...
```

```
telnet localhost 6000
helo test.com
mail from: test@test.com
rcpt to: marc.maeurer@pm.me
data
subject: this is a test message
sending a test message via telnet
.
```















# no i should be able to login like that
username@example.com
username@molotow.com

