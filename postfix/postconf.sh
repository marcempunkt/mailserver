#!/bin/bash

### Base configuration + user authentication method
## set hostname
postconf -e myhostname="mail.molotow.com" ## TODO change it to env var
postconf -e mydomain="molotow.com"
## restrict access from only this ip addresses
# postconf -e mynetworks="127.0.0.0/8, 168.100.189.0/28"

### Don't accept any incoming mail 
postconf -e mailbox_size_limit=0
postconf -e recipient_delimiter="+"
# postconf -e inet_interfaces="loopback-only"

### SASL
postconf -e smtpd_sasl_auth_enable="yes"
postconf -e smtpd_recipient_restrictions="permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination"
## Support for old email clients (may not be needed)
postconf -e broken_sasl_auth_clients="yes"
## set the maildirectory
postconf -e home_mailbox="maildir/"
## update db
newaliases

## Configure postfix to us tls certs
postconf -e smtpd_use_tls="yes"
postconf -e smtpd_tls_auth_only="yes"
postconf -e smtpd_tls_key_file="/etc/postfix/certs/privkey.pem"
postconf -e smtpd_tls_cert_file="/etc/postfix/certs/cert.pem"
postconf -e smtp_tls_security_level="may"
postconf -e smtpd_tls_security_level="may"
postconf -e smtpd_tls_loglevel=1


