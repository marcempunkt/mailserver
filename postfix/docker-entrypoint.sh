#!/bin/bash
## Set authentication method to pam
saslauthd -a pam

### SSL/TSL
if [ ! -d "etc/postfix/certs" ] && [ ! -f "etc/postfix/certs/privkey.pem" ] && [ ! -f "etc/postfix/certs/cert.pem" ]; then
    echo
    echo "### No SSL Certs found! Will now generate self-signed certificates."
    mkdir -p /etc/postfix/certs/
    ## Generate & sign ssl/tsl certs with openssl
    openssl req -x509 -nodes -newkey rsa:4096 -days 1 \
	    -keyout '/etc/postfix/certs/privkey.pem' \
	    -out '/etc/postfix/certs/cert.pem' \
	    -subj '/CN=localhost'
    echo
fi

## Start postfix in foreground,
## so that the docker container won't exit immediately after starting it
postfix start-fg
